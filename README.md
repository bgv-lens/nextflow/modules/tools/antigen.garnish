# antigen.garnish

antigen.garnish Nextflow Module for use iwth RAFT

Website: https://github.com/andrewrech/antigen.garnish

Manuscript: https://www.cell.com/cell-systems/pdfExtended/S2405-4712(19)30307-2

| workflow | container | cpus | memory |
| --- | --- | --- | --- |
| antigen_garnish_foreignness | docker://andrewrech/antigen.garnish:2.3.0 | 1 * task.attempt | 4.GB.plus(4.GB * task.attempt) |
| antigen_garnish_foreignness_rna | docker://andrewrech/antigen.garnish:2.3.0 | 1 * task.attempt | 4.GB.plus(4.GB * task.attempt) |
| antigen_garnish_dissimilarity | docker://andrewrech/antigen.garnish:2.3.0 | 1 * task.attempt | 4.GB.plus(4.GB * task.attempt) |
| antigen_garnish_dissimilarity_rna | docker://andrewrech/antigen.garnish:2.3.0 | 1 * task.attempt | 4.GB.plus(4.GB * task.attempt) |
| antigen_garnish_make_input_file | N/A | N/A | N/A |

## Workflows

# antigen_garnish_foreignness
Runs antigen.garnish foreignness calculation
```
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(norm_run) - Normal Run Name
//     val(tumor_run) - Tumor Run Name
//     val(dataset) - Dataset
//     path(peptides_file) - Peptides File
//   path(ag_data_dir) - antigen.garnish Data Directory
//   val(species) - Sample Species
//
// output:
//   tuple => foreignness_files
//     val(pat_name) - Patient Name
//     val(norm_run) - Normal Run Name
//     val(tumor_run) - Tumor Run Name
//     val(dataset) - Dataset
//     path("*agg.ag_foreign.tsv") - antigen.garnish Foreignness File
```

# antigen_garnish_foreignness_rna
Runs antigen.garnish foreignness calculation
```
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(norm_run) - Run Name
//     val(dataset) - Dataset
//     path(peptides_file) - Peptides File
//   path(ag_data_dir) - antigen.garnish Data Directory
//   val(species) - Sample Species
//
// output:
//   tuple => foreignness_files
//     val(pat_name) - Patient Name
//     val(tumor_run) - Run Name
//     val(dataset) - Dataset
//     path("*agg.ag_foreign.tsv") - antigen.garnish Foreignness File
```

# antigen_garnish_dissimilarity
Runs antigen.garnish dissimilarity calculation
```
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(norm_run) - Normal Run Name
//     val(tumor_run) - Tumor Run Name
//     val(dataset) - Dataset
//     path(peptides_file) - Peptides File
//   path(ag_data_dir) - antigen.garnish Data Directory
//   val(species) - Sample Species
//
// output:
//   tuple => foreignness_files
//     val(pat_name) - Patient Name
//     val(norm_run) - Normal Run Name
//     val(tumor_run) - Tumor Run Name
//     val(dataset) - Dataset
//     path("*agg.ag_dissim.tsv") - antigen.garnish Dissimilarity File
```

# antigen_garnish_dissimilarity_rna
Runs antigen.garnish dissimilarity calculation
```
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(peptides_file) - Peptides File
//   path(ag_data_dir) - antigen.garnish Data Directory
//   val(species) - Sample Species
//
// output:
//   tuple => foreignness_files
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path("*agg.ag_dissim.tsv") - antigen.garnish Dissimilarity File
```

# antigen_garnish_make_input_file
Makes antigen.garnish input file
```
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(norm_run) - Normal Run Name
//     val(tumor_run) - Tumor Run Name
//     val(dataset) - Dataset
//     path(peptides_file) - Peptides File
//   val(comma_sep_lens) - Comma-separated List of Peptide Lengths
//
// output:
//   tuple => foreignness_files
//     val(pat_name) - Patient Name
//     val(norm_run) - Normal Run Name
//     val(tumor_run) - Tumor Run Name
//     val(dataset) - Dataset
//     path("*ag_inp.tsv") - antigen.garnish Input File
```
